#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
import sys


def robot_move(message):
    rospy.init_node('turtle_moving')
    pub = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    rate = rospy.Rate(10)  # 10
    turtle_velocity = Twist()
    
    if message == "move_forward":
        while not rospy.is_shutdown():
            turtle_velocity.linear.x = 2.0
            turtle_velocity.linear.y = 0
            turtle_velocity.linear.z = 0
            turtle_velocity.angular.x = 0
            turtle_velocity.angular.y = 0
            turtle_velocity.angular.z = 0
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
            pub.publish(turtle_velocity)
            rate.sleep()

    elif message == "move_backward":
        while not rospy.is_shutdown():
            turtle_velocity.linear.x = -2.0
            turtle_velocity.linear.y = 0
            turtle_velocity.linear.z = 0
            turtle_velocity.angular.x = 0
            turtle_velocity.angular.y = 0
            turtle_velocity.angular.z = 0
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
            pub.publish(turtle_velocity)
            rate.sleep()

    elif message == "turn_left":
        while not rospy.is_shutdown():
            turtle_velocity.linear.x = 0.0
            turtle_velocity.linear.y = 0
            turtle_velocity.linear.z = 0
            turtle_velocity.angular.x = 0
            turtle_velocity.angular.y = 0
            turtle_velocity.angular.z = 2.0
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
            pub.publish(turtle_velocity)
            rate.sleep()

    elif message == "turn_right":
        while not rospy.is_shutdown():
            turtle_velocity.linear.x = 0.0
            turtle_velocity.linear.y = 0
            turtle_velocity.linear.z = 0
            turtle_velocity.angular.x = 0
            turtle_velocity.angular.y = 0
            turtle_velocity.angular.z = -2.0
            rospy.loginfo("Linear turtle velocity = %f, angular turtle velocity = %f", turtle_velocity.linear.x, turtle_velocity.angular.z)
            pub.publish(turtle_velocity)
            rate.sleep()


if __name__ == '__main__':
    try:
        robot_move(str(sys.argv[1]))
    except rospy.ROSInterruptException:
        pass
#